#include<stdio.h>
#include<string.h>

struct student {

  char name[20];
  char subject[20];
  float marks;
};

struct student getinfo(){

  struct student S;

  printf("Enter name of student:");
  scanf("%s",S.name);
  
  printf("Enter subject:");
  scanf("%s",S.subject);
  
  printf("Enter marks of the student:");
  scanf("%f",&S.marks);
  
  if (S.marks<0 || S.marks>100){
  	
  	printf("Invalid Marks\n");
  	printf("Enter marks of the student:");
  	scanf("%f",&S.marks);
  }
  
  return S;
}
int main() {

  struct student arr_student[5];
  int i;
  
  for(i=0;i<5;i++){
  	
  	arr_student[i]= getinfo();
  	
  }
  
  printf("\n");  
	
  printf("Name\tsubject\tMarks\n");

  for(i=0; i<5; i++ )
  {
    printf("%s\t%s\t%.2f\n",arr_student[i].name,arr_student[i].subject,arr_student[i].marks);
  }

  return 0;
}
