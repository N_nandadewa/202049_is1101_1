#include<stdio.h>
#include<string.h>

int main(){
	
	char sentence[100], reverse[100];
    int len, i, index, wordStart, wordEnd;

    printf("Enter any sentence: ");
    gets(sentence);

    len   = strlen(sentence);
   
    index = 0;

    
    wordStart = len - 1;
    
    wordEnd   = len - 1;
    

    while(wordStart > 0)
    {
        
        if(sentence[wordStart] == ' ')
        {
            
            
            i = wordStart + 1;
            
            while(i <= wordEnd)
            {
                reverse[index] = sentence[i];

                i++;
                index++;
            }
            reverse[index++] = ' ';

            wordEnd = wordStart - 1;
        }

        wordStart--;
    }

    
    for(i=0; i<=wordEnd; i++)
    {
        reverse[index] = sentence[i];
        index++;
    }

    
    reverse[index] = '\0'; 

    printf("Original sentence \n%s\n\n", sentence);
    printf("Reversed sentence \n%s", reverse);

    return 0;
}
