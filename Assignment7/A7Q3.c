#include<stdio.h>

int matrix_1[4][4];
int matrix_2[4][4];
int add_matrix[4][4];
int mul_matrix[4][4];

int i,j,n;

int r1,c1; // number of rows and columns of matrix_1;

int r2,c2; // number of rows and columns of matrix_2

int Ar,Ac; // number of rows and columns of add_matrix 

int Mr,Mc; // number of rows and columns of mul_matrix 

void TakeInput(){
	
	printf("Input number of rows and columns of matrix_1: ");
	scanf("%d %d",&r1,&c1);
	
	printf("Input number of rows and columns of matrix_2: ");
	scanf("%d %d",&r2,&c2);
	
	printf("\n");
	
	for(i=0;i<r1;i++){
		for(j=0;j<c1;j++){
			
			printf("matrix 1: matrix_1[%d][%d]: ",i,j);
			scanf("%d",&matrix_1[i][j]);
		}
	}
	
	printf("\n");
	
	for(i=0;i<r2;i++){
		for(j=0;j<c2;j++){
			
			printf("matrix 2: matrix_2[%d][%d]: ",i,j);
			scanf("%d",&matrix_2[i][j]);
		}
	}
	
	printf("\n");
		
	
}


void MULmatrix( int matrix_1[4][4],int matrix_2[4][4]){
	
	if(c1==r2){
		
		Mr = r1;
		Mc = c2;
		
		for(i = 0; i < r1  ;i++){
		
			for(j = 0; j < c2; j++){
			
				mul_matrix[i][j]=0;
			
				for(n = 0; n < Mc ; n++){
				
					mul_matrix[i][j]+= matrix_1[i][n]*matrix_2[n][j];
				}			
			}
		}
			
		printf("Result matrix is (matrix_1 * matrix_2) \n"); 
		printf("\n");
    	for (i = 0; i < Mr; i++) 
    	{ 
        	for (j = 0; j < Mc; j++){
		
           		printf("%d ", mul_matrix[i][j]); 
        		
    		}
    		printf("\n"); 
    	}
    
    	printf("\n");	

	}
	
	else if (c2==r1){
		
		Mr=r2;
		Mc=c1;
	
	
		for(i = 0; i < r2  ;i++){
		
			for(j = 0; j < c1; j++){
			
				mul_matrix[i][j]=0;
			
				for(n = 0; n < Mc; n++){
				
					mul_matrix[i][j]+= matrix_2[i][n]*matrix_1[n][j];
				}		
			}
		}
			
		printf("Result matrix is (matrix_1 * matrix_2) \n"); 
		printf("\n");
    	for (i = 0; i < Mr; i++) 
    	{ 
        	for (j = 0; j < Mc; j++){
		
           		printf("%d ", mul_matrix[i][j]); 
        		
    		}
    	
			printf("\n"); 
    	}
    
    	printf("\n");	

	}
	
	else 
		printf("can not multiply these matrices!\n");
	

}

void ADDmatrix( int matrix_1[4][4],int matrix_2[4][4]){
	
		if(r1==r2 && c1==c2){
			
			Ar= r1;
			Ac= c1;
		
			for(i = 0; i < Ar ; i++){
		
				for(j = 0; j < Ac; j++){
			
				add_matrix[i][j]=0;
			
				add_matrix[i][j]+= matrix_2[i][j]+matrix_1[i][j];
					
				}	
			}
			
			printf("Result matrix is (matrix_1 + matrix_2) \n"); 
			printf("\n");
    		for (i = 0; i < Ar; i++) 
    		{ 
        		for (j = 0; j < Ac; j++) {
				
           			printf("%d ", add_matrix[i][j]); 
           		}
           		printf("\n"); 
    		} 
	}
	else
	 printf("can not add these matrices!");
	
	
}
	

int main(){
	
	

	TakeInput();	
	
	MULmatrix(matrix_1,matrix_2);
	
	ADDmatrix(matrix_1,matrix_2);
	
	return 0; 
	
}
