#include<stdio.h>
int income(int ticket,int attendees){
	
	return ticket*attendees;
}

int expenditure(int attendees){
	int fixed_cost=500;
	int cost_per_attendee=3;
	return fixed_cost+cost_per_attendee*attendees;
}

int Num_of_attendees(int num_of_attendees){
	
	num_of_attendees=num_of_attendees-20;
	return num_of_attendees;
	
	 
}

int profit(int income,int expenditure){
	
	return income-expenditure;
}


int main(){
	
	int final_ticket_price,ticket_price=15;
	int attendees=120;
	int max_profit=0,net_profit=0;
	int revenue,cost;
	
	
	while(attendees>=20){
		
		revenue=income(ticket_price,attendees);
		
		cost =expenditure(attendees);
		
		net_profit=profit(revenue,cost);
	
		if (max_profit<net_profit){
		
	
			max_profit=net_profit;
			final_ticket_price=ticket_price;
			
		}
		else
			break;
	
		ticket_price=ticket_price+5;
		attendees=Num_of_attendees(attendees);	
		
	
	}
	printf("optimized ticket price %d\n",final_ticket_price);
	return 0;

}
